# Use nginx unprivileged (non-root) image as the base image
FROM nginxinc/nginx-unprivileged

# Copy the build output to replace the default nginx contents.
COPY shippers.html /usr/share/nginx/html/index.html
COPY js /usr/share/nginx/html/js

# Expose port 8080
EXPOSE 8080

# No need for a CMD or ENTRYPOINT as nginx will run from the base image