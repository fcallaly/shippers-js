const url = "http://franks-shippers-demo-franks-shippers-demo.emeadocker65.conygre.com:80/api/v1/shipper";

// define a function to retrieve the list of shippers
function getShippers() {

    fetch(url)
        .then(response => {
            console.log(response);
            console.log(response.status);
            return response.json();
        })
        .then(shippersJson => {
            console.log(shippersJson);
            let htmlString = "";

            shippersJson.forEach(shipper => {
                htmlString += '<tr><td id="name-shipper-' + shipper.id + '">' + shipper.companyName + '</td>';
                htmlString += '<td>' + shipper.phone + '</td>';
                htmlString += '<td><button class="delete-shipper" id="del-shipper-'
                htmlString += shipper.id + '">&#10060;</button></td></tr>'
            });

            document.querySelector("#shippers-table").innerHTML = htmlString;

            // we just added a button in each table row, each button has class delete-shipper,
            // attach the click event of each button to the deleteShipper function
            document.querySelectorAll(".delete-shipper").forEach(
                (thisButton) => thisButton.addEventListener('click', deleteShipper));
        })
        // If there is an error
        .catch((error) => {
            console.error('Error:', error);
        });
}

// connect the click event of button-get to the above function
document.querySelector("#button-get").addEventListener('click', getShippers);


// function to send a POST to create a new record
function createShipper() {
    const data = {
        companyName: document.getElementById('name').value,
        phone: document.getElementById('phone').value
    };
    //POST request with body equal on data in JSON format
    fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        })
        // Then read the body of the response as json
        .then((response) => response.json())
        // Then re-read all shippers as we expect a new entry in the list
        .then((data) => {
            console.log('Create Success: ', data);
            alert("New record saved: " + data.companyName);
            getShippers();
        })
        // If there is an error
        .catch((error) => {
            console.error('Error: ', error);
        });
}

// connect the click event of button-save to the above function
document.querySelector("#button-create").addEventListener('click', createShipper);

// Takes a click event on a shipper delete button, extracts the id, finds the shipper name
// Asks for confirmation from the user, if confirmed then call deleteShipperById
function deleteShipper(event) {
    let shipperId = event.target.id.substring(12);
    let shipperName = document.querySelector("#name-shipper-" + shipperId).innerHTML;

    console.log(shipperName);

    if (confirm('Are you sure you want to delete ' + shipperName + '?')) {
        // Save it!
        deleteShipperById(event.target.id.substring(12));
    } else {
        // Do nothing!
        console.log('Not deleting');
    }

}

// delete a shipper by Id
function deleteShipperById(id) {
    console.log("Deleting shipper id: " + id);

    //POST request with body equal on data in JSON format
    fetch(url + "/" + id, {
            method: 'DELETE',
        })
        // Then print some logs and reload the list of shippers
        .then((response) => {
            console.log("Delete response:")
            console.log(response);
            getShippers();
        })
        // If there is an error
        .catch((error) => {
            console.error('Error:', error);
        });
}

// Initially fill the list on page load
getShippers();